import logging
import sys

import aiohttp.web
from aiojson.middleware import error_middleware
from aiojson.routes import routes

from dataserver import api, config

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


async def start_api(host, port):
    api  # for code reformat
    app = aiohttp.web.Application(middlewares=[error_middleware])
    app.add_routes(routes)
    runner = aiohttp.web.AppRunner(app)
    await runner.setup()
    site = aiohttp.web.TCPSite(runner, host=host, port=port)
    await site.start()


async def main():
    await start_api(*config.server_info)

config.loop.run_until_complete(main())

if __name__ == "__main__":
    config.loop.run_forever()
