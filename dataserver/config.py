# Configuration module. All environment values names should be upper case.
import asyncio
import os
from motor.motor_asyncio import AsyncIOMotorClient

loop = asyncio.get_event_loop()

# Server config
SERVER_HOST = os.environ.get("SERVER_HOST", "0.0.0.0")
SERVER_PORT = os.environ.get("SERVER_PORT", 2108)
server_info = (SERVER_HOST, SERVER_PORT)

# database config
MONGO_URI = os.environ.get("MONGO_URI", "localhost")
DATABASE_NAME = os.environ.get("DATABASE_NAME", "L2pyData")
mongo_client = AsyncIOMotorClient(MONGO_URI)
