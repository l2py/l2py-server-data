from dataserver.config import DATABASE_NAME, mongo_client
from dataserver.models.api_document import ApiDocument


class OtherItem(ApiDocument):
    """Game item."""

    collection = "items"
    database = DATABASE_NAME
    client = mongo_client

    structure = {
        "_id": int,
        "name": str,
        "item_type": int,
        "weight": int,
        "crystal_type": int,
        "duration": int,
        "price": int,
        "crystal_count": int,
        "crystallizable": bool,
        "sellable": bool,
        "dropable": bool,
        "destroyable": bool,
        "tradeable": bool,
        "stackable": bool,
    }
