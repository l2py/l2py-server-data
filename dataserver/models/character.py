import time

import pymongo

from dataserver.config import DATABASE_NAME, mongo_client
from dataserver.models.account import Account
from dataserver.models.api_document import ApiDocument


class Character(ApiDocument):
    collection = "characters"
    database = DATABASE_NAME
    client = mongo_client

    structure = {
        "name": str,
        "account": str,
        "clan_id": int,
        "sex": int,
        "race": int,
        "pos_x": int,
        "pos_y": int,
        "pos_z": int,
        "hp": int,
        "max_hp": int,
        "mp": int,
        "max_mp": int,
        "sp": int,
        "max_sp": int,
        "exp": int,
        "epx_before_death": int,
        "level": int,
        "karma": int,
        "hair_color": int,
        "hair_style": int,
        "pvp_kills": int,
        "pk_kills": int,
        "class_id": int,
        "base_class": int,
        "delete_time": int,
        "can_craft": bool,
        "title": str,
        "recommends_have": int,
        "recommends_left": int,
        "is_online": bool,
        "online_time": int,
        "access_level": int,
        "character_slot": int,
        "last_access": int,
        "punish_time": int,
        "power_grade": int,
        "nobless": bool,
        "hero": bool,
        "last_recommend_date": int,
        "__required__": ["__all__"]
    }

    @classmethod
    def create_indexes(cls):
        """Creates required indexes."""

        cls.sync_collection.create_index([
            ("name", pymongo.ASCENDING)
        ], unique=True)

        cls.sync_collection.create_index([
            ("account", pymongo.ASCENDING)
        ])

    @ApiDocument.related(Account.Field._id, multiple=False, parent=False,
                         other_is_parent=True)
    async def account(self):
        pass

    async def mark_deleted(self):
        self.deleted = int(time.time())
        await self.push_update()
