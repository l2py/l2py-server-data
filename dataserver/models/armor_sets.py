import pymongo

from dataserver.config import DATABASE_NAME, mongo_client
from dataserver.models.api_document import ApiDocument


class ArmorSet(ApiDocument):
    """Armor sets."""

    collection = "armor_sets"
    database = DATABASE_NAME
    client = mongo_client

    structure = {
        "id": int,
        "chest_id": int,
        "legs_id": int,
        "head_id": int,
        "gloves_id": int,
        "feet_id": int,
        "shield_id": int,
        "skill_id": int,
        "shield_skill_id": int,
        "enchant6skill_id": int,
        "__required__": ["__all__"],
    }

    @classmethod
    def create_indexes(cls):
        cls.sync_collection.create_index([
            ("id", pymongo.ASCENDING)
        ], unique=True)
