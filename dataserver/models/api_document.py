from mdocument import Document, MetaDocument


class ApiMetaDocument(MetaDocument):
    structure: dict

    @property
    def _structure(cls):
        return {key: value for key, value in cls.structure.items()
                if not key.startswith("__")}


class ApiDocument(Document, metaclass=ApiMetaDocument):
    structure: dict

    @classmethod
    async def create(cls, **kwargs) -> "Document":
        query = {key: kwargs.get(key) for key in cls._structure.items()}

        return await super().create(**query)
