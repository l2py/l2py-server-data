import pymongo
from mdocument import Document

from dataserver.config import DATABASE_NAME, mongo_client
from dataserver.models.api_document import ApiDocument
from dataserver.models.character import Character


class Item(ApiDocument):
    """Game item."""

    collection = "items"
    database = DATABASE_NAME
    client = mongo_client

    structure = {
        "character": str,
        "object_id": int,
        "count": int,
        "enchant": int,
        "location": int,
        "location_data": int,
        "time_of_use": int,
        "custom_type1": int,
        "custom_type2": int,
        "mana_left": int,
        "time": int,
        "__required__": ["__all__"],
    }

    @classmethod
    def create_indexes(cls):
        cls.sync_collection.create_index([
            ("object_id", pymongo.ASCENDING)
        ])

        cls.sync_collection.create_index([
            ("character", pymongo.ASCENDING)
        ])

    @Document.related(Character.Field._id, other_is_parent=True,
                      parent=False, multiple=False)
    async def character(self):
        pass
