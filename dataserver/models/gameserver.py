import time

import pymongo
from mdocument import Document

from dataserver.config import DATABASE_NAME, mongo_client


class GameServer(Document):
    """GameServer."""

    collection = "game_servers"
    database = DATABASE_NAME
    client = mongo_client

    structure = {
        "id": int,
        "ip": str,
        "port": int,
        "age_limit": int,
        "is_pvp": bool,
        "online": int,
        "max_online": int,
        "is_online": bool,
        "server_type": int,
        "public": bool,
        "brackets": bool,
        "latest_ping": float,
        "__required__": ["__all__"]
    }

    @classmethod
    def create_indexes(cls):
        cls.sync_collection.create_index([
            ("id", pymongo.ASCENDING)
        ], unique=True)

    @property
    def ip(self):
        ip = self._document_.get("ip", "127.0.0.1")
        if len(ip.split(".")) != 4:
            ip = "127.0.0.1"
        return [int(sip) for sip in ip.split(".")]

    @property
    def is_online(self):
        return bool(self._document_.get("is_online", False))

    @classmethod
    async def create(cls, **kwargs) -> "Document":

        query = {
            "id": kwargs["id"],
            "ip": kwargs["ip"],
            "port": kwargs["port"],
            "age_limit": kwargs.get("age_limit", 0),
            "is_pvp": kwargs.get("is_pvp", False),
            "online": 0,
            "max_online": 500,
            "is_online": False,
            "server_type": kwargs.get("server_type", 1),
            "public": kwargs.get("public", True),
            "brackets": kwargs.get("brackets", False),
            "latest_ping": 0,
        }

        return await super().create(**query)

    @classmethod
    async def save_ping(cls, id):
        cls.collection.update_one({"id": id}, {"$set": {"latest_ping": time.time()}})

    def to_json(self):
        json = super().to_json()
        if time.time() - self.latest_ping > 30:
            json["is_online"] = False
        else:
            json["is_online"] = True
        return json
