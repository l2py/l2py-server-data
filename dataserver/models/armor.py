from dataserver.config import DATABASE_NAME, mongo_client
from dataserver.models.api_document import ApiDocument


class Armor(ApiDocument):
    """Game Armor."""

    collection = "armors"
    database = DATABASE_NAME
    client = mongo_client

    structure = {
        "id": int,
        "name": str,
        "body_part": int,
        "crystallizable": bool,
        "armor_type": int,
        "weight": int,
        "avoid_modify": bool,
        "duration": int,
        "pdef": int,
        "mdef": int,
        "mp_bonus": int,
        "price": int,
        "crystal_type": int,
        "crystal_count": int,
        "sellable": bool,
        "dropable": bool,
        "destoryable": bool,
        "tradeable": bool,
        "item_skill_id": int,
        "item_skill_lvl": int,
        "__required__": ["__all__"],
    }
