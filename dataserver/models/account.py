import base64
import datetime
import os
import time
from hashlib import sha3_512

import pymongo
from mdocument import DocumentDoesntExist

from dataserver.config import DATABASE_NAME, mongo_client
from dataserver.models.api_document import ApiDocument


class Account(ApiDocument):
    """LoginServer user account."""

    collection = "accounts"
    database = DATABASE_NAME
    client = mongo_client

    structure = {
        "login": str,
        "password": str,
        "salt": str,
        "access_level": int,
        "last_active": int,
        "banned": None,
        "banned_until": None,
        "deleted": None,
        "latest_server": int,
        "__required__": ["login", "password", "access_level"]
    }

    public_fields = ["login", "banned", "deleted", "latest_server"]

    @classmethod
    def create_indexes(cls):
        """Creates required indexes."""

        cls.sync_collection.create_index([
            ("login", pymongo.ASCENDING)
        ], unique=True)

    @classmethod
    async def create(cls, login, password, email=None, access_level=0) -> "Document":
        """Registers new user."""

        hashed_password, salt = cls.hash_password(password)

        query = {
            "login": login,
            "password": hashed_password,
            "salt": salt,
            "latest_server": 0,
            "access_level": access_level,
        }

        if email is not None:
            query["email"] = email

        return await super().create(**query)

    @staticmethod
    def hash_password(password, salt=None) -> tuple:
        """Hashes plain text password."""

        salt = base64.urlsafe_b64encode(os.urandom(50)).decode() if not salt else salt
        salt = salt[::-1]
        hashed = sha3_512(password.encode())
        hashed.update(salt.encode())
        return base64.urlsafe_b64encode(hashed.digest()).decode(), salt[::-1]

    def check_password(self, password):
        """Checks that provided password matches hashed in db."""

        hashed_password, salt = self.hash_password(password, self.salt)
        if hashed_password == self.password:
            return True
        return False

    @classmethod
    async def authenticate(cls, login, password):
        """Authenticates account using password."""

        try:
            account = await cls.one(login=login)
            if account.check_password(password) and not getattr(account, "deleted", False):
                return account
        except DocumentDoesntExist:
            return

    @property
    def can_login(self):
        return not bool(getattr(self, "banned", False))

    async def ban(self, duration_minutes):
        """Marks account as banned."""

        now = time.time()
        self.banned = now
        self.banned_until = now + datetime.timedelta(minutes=duration_minutes).seconds
        await self.push_update()

    async def unban(self):
        """Removes banned mark."""

        if getattr(self, "banned", False):
            delattr(self, "banned")
        if getattr(self, "banned_until", False):
            delattr(self, "banned_until")
        await self.push_update()

    @property
    def latest_server(self):
        return self._document_.get("latest_server", 0)

    @property
    def public_info(self):
        return {key: value for key, value in self._document_.items() if key in self.public_fields}

    async def mark_deleted(self):
        """Marks account as deleted."""

        self.deleted = time.time()
        await self.push_update()

    @property
    def last_active(self):
        return self._document_.get("last_active", 0)
