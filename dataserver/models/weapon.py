from dataserver.config import DATABASE_NAME, mongo_client
from dataserver.models.api_document import ApiDocument


class Weapon(ApiDocument):
    """Game weapon."""

    collection = "items"
    database = DATABASE_NAME
    client = mongo_client

    structure = {
        "_id": int,
        "name": str,
        "body_part": int,
        "crystallizable": bool,
        "weight": int,
        "soulshots": int,
        "spiritshots": int,
        "crystal_type": int,
        "physical_damage": int,
        "random_damage": int,
        "weapon_type": int,
        "critical": int,
        "hit_modify": int,
        "avoid_modify": int,
        "shield_defense": int,
        "shield_defense_rate": int,
        "attack_speed": int,
        "mana_consumption": int,
        "magic_damage": int,
        "duration": int,
        "price": int,
        "crystal_count": int,
        "sellable": bool,
        "dropable": bool,
        "destroyable": bool,
        "tradeable": bool,
        "item_skill_id": int,
        "item_skill_level": int,
        "enchant4_skill_id": int,
        "enchant4_skill_level": int,
        "on_cast_skill_id": int,
        "on_cast_skill_level": int,
        "on_cast_skill_chance": int,
        "on_critical_skill_id": int,
        "on_critical_skill_level": int,
        "on_critical_skill_chance": int,
        "__required__": ["__all__"],
    }
