from dataserver.config import DATABASE_NAME, mongo_client
from dataserver.models.api_document import ApiDocument


class Spawn(ApiDocument):
    """Game creature spawn properties."""

    collection = "items"
    database = DATABASE_NAME
    client = mongo_client

    structure = {
        "template_id": int,
        "location_x": int,
        "location_y": int,
        "location_z": int,
        "heading": int,
        "respawn_delay": int,
        "respawn_random_range": int,
        "period_of_day": int,
        "__required__": ["__all__"],
    }
