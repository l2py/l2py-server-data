import pymongo
from mdocument import Document

from dataserver.config import DATABASE_NAME, mongo_client
from dataserver.models.character import Character


class CharacterSkill(Document):
    """Character skill."""

    collection = "character_skills"
    database = DATABASE_NAME
    client = mongo_client

    structure = {
        "character": str,
        "skill_id": int,
        "skill_level": int,
        "class_id": int,
        "__required__": ["__all__"],
    }

    @classmethod
    def create_indexes(cls):
        """Creates required indexes."""

        cls.sync_collection.create_index([
            ("character", pymongo.ASCENDING)
        ])

    @classmethod
    async def create(cls, **kwargs) -> "CharacterSkill":
        """Creates new character skill."""

        query = {key: kwargs[key] for key in cls.structure}

        if None in query.values():
            raise ValueError("Value cant be None")

        return await super().create(**query)

    @Document.related(Character.Field._id, other_is_parent=True,
                      multiple=False, parent=False)
    async def character(self):
        pass
