import time

from dataserver.config import DATABASE_NAME, mongo_client
from dataserver.models.api_document import ApiDocument


class Announcement(ApiDocument):
    """Server announcement."""

    collection = "announcements"
    database = DATABASE_NAME
    client = mongo_client

    structure = {
        "text": str,
        "type": int,
        "deleted": None,
        "__required__": ["text", "type"],
    }

    async def mark_deleted(self):
        self.deleted = time.time()
        await self.push_update()

    @classmethod
    async def describe(cls):
        """Finds all active announcements."""

        return await cls.many(deleted={"$exists": False})
