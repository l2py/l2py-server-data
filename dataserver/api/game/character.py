from aiojson import JsonTemplate
from aiojson.routes import delete, get, post
from common.client import exceptions
from mdocument import document

from dataserver.models.character import Character


@get("/game/character")
@JsonTemplate({
    "id": int,
    "__required__": ["__all__"],
})
async def get_character(request, validated_data):
    """Finds one character in database."""

    try:
        character = await Character.one(id=validated_data["id"])
        if character:
            return character.to_json()
    except document.DocumentDoesntExist:
        raise exceptions.DocumentDoesntExist(
            f"Character with id {validated_data['_id']} doesn't exist") from None


@post("/game/character")
@JsonTemplate(Character.structure)
async def create_character(request, validated_data):
    """Creates new character."""

    character = await Character.create(**validated_data)
    return character.to_json()


@delete("/game/character")
@JsonTemplate({
    "id": int,
    "__required__": ["id"]
})
async def delete_character(request, validated_data):
    """Marks game character for deletion."""

    try:
        character = await Character.one(id=validated_data["id"])
    except document.DocumentDoesntExist:
        raise exceptions.DocumentDoesntExist(
            f"Character with id {validated_data['_id']} doesn't exist") from None

    await character.mark_deleted()
    return True
