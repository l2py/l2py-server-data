import logging

from aiojson import JsonTemplate
from aiojson.routes import delete, get, post

from dataserver.models.gameserver import GameServer

LOG = logging.getLogger(f"L2py_data.{__name__}")


@post("/game/server")
@JsonTemplate(GameServer.structure)
async def create_gameserver(request, validated_data):
    """Creates new game server."""

    await GameServer.create(**{
        "id": validated_data["id"],
        "ip": str(validated_data["ip"]),
        "port": validated_data["port"]
    })
    return True


@get("/game/server")
@JsonTemplate({
    "id": int,
    "__required__": ["id"]
})
async def get_server(request, validated_data):
    """Finds one game server."""

    server = await GameServer.one(id=validated_data["id"])
    return server.to_json()


@delete("/game/server")
@JsonTemplate({
    "id": int,
    "__required__": ["id"]
})
async def delete_server(request, validated_data):

    server = await GameServer.one(id=validated_data["id"])
    await server.delete()
    return True


@get("/game/server_list")
async def gameservers_list(request):

    return [server.to_json() for server in await GameServer.many()]


@post("/game/server.ping")
@JsonTemplate({
    "id": int,
    "__required__": ["id"]
})
async def game_server_ping(request, validated_data):

    await GameServer.save_ping(validated_data["id"])
