# monkeypatch exceptions in common.client.exceptions so same exceptions
# are raised on clients

import aiojson.exception
import common.client.exceptions


aiojson.exception.ApiException.__bases__ = (BaseException, )
common.client.exceptions.ApiException.__init__ = aiojson.exception.ApiException.__init__
common.client.exceptions.ApiException.__bases__ += (aiojson.exception.ApiException,)
