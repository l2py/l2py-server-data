import logging

from aiojson import JsonTemplate
from aiojson.routes import delete, get, post
from common.datatypes import Int32, Int8, String
from common.client import exceptions
from dataserver.models.account import Account

LOG = logging.getLogger(f"L2py_data.{__name__}")


@post("/login/account")
@JsonTemplate({
    "login": String,
    "password": String,
    "email": String,
    "__required__": ["login", "password"],
})
async def create_account(request, validated_data):
    """Creates new account."""

    await Account.create(**{key: value.value for key, value in validated_data.items()})


@get("/login/account")
@JsonTemplate({
    "login": String,
    "password": String,
    "__required__": ["login", "password"],
})
async def get_account(request, validated_data):
    """Gets information about account."""

    account = await Account.authenticate(validated_data["login"].value,
                                         validated_data["password"].value)
    if account:
        return account.public_info
    else:
        raise exceptions.WrongCredentials(
            f"Wrong credentials for account {validated_data['login']}")


@delete("/login/account")
@JsonTemplate({
    "login": String,
    "__required__": ["login"],
})
async def delete_account(request, validated_data):
    """Marks account as deleted."""

    account = await Account.one(login=validated_data["login"])
    await account.mark_deleted()
    return True


@post("/login/account.ban")
@JsonTemplate({
    "login": String,
    "duration_minutes": Int32,
    "__required__": ["login", "duration_minutes"],
})
async def ban_account(request, validated_data):
    """Banns account for specified time."""

    account = await Account.one(login=validated_data["login"])
    await account.ban(validated_data["duration_minutes"])
    return True


@post("/login/account.unban")
@JsonTemplate({
    "login": String,
    "__required__": ["login"],
})
async def unban_account(request, validated_data):
    """Removes banned mark from account."""

    account = await Account.one(login=validated_data["login"])
    await account.unban()
    return True


@post("/login/account.set_latest_server")
@JsonTemplate({
    "login": String,
    "server_id": Int8,
    "__required__": ["login", "server_id"],
})
async def set_account_latest_server(request, validated_data):
    """Saves latest connected server to account info."""

    account = await Account.one(login=validated_data["login"].value)
    account["latest_server"] = validated_data["server_id"].value
    await account.push_update()
    return True
