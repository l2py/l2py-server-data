FROM python:3.8
ADD dataserver /code/l2py_data/dataserver
ADD requirements.txt /code/l2py_data/
ADD setup.py /code/l2py_data/
WORKDIR /code/l2py_data/
RUN pip install -r requirements.txt
RUN python setup.py install
ENV PYTHONBUFFERED 1
CMD python dataserver/runner.py
